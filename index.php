<?php

$WatchOne = ['https://cdn11.bigcommerce.com/s-e31c8/images/stencil/1280x1280/products/847/14935/EQB_510DC_1AER__24441.1431166798__61898.1561630803.jpg?c=2', 'Casio Edifice', 'This is a watch. A brand new Watch from VW. Waterproof, stainless steel !', 630.99, 0.5, 15, 4.6, 'female', 0];
$WatchOne[9] = $WatchOne[3] - ($WatchOne[3] * $WatchOne[4]);
$WatchOne[10] = $WatchOne[9] + $WatchOne[5];

$WatchTwo = ['https://www.brandfield.com/media/catalog/product/cache/image/9df78eab33525d08d6e5fb8d27136e95/a/1/a168wegg-1bef_print.jpg', 'Casio Edifice', 'This is a watch. A brand new Watch from VW. Waterproof, stainless steel !', 150, 0.3, 15, 4.6, 'Male', 49];
$WatchTwo[9] = $WatchTwo[3] - ($WatchTwo[3] * $WatchTwo[4]);
$WatchTwo[10] = $WatchTwo[9] + $WatchTwo[5];

$WatchThree = ['https://images-na.ssl-images-amazon.com/images/I/91A8bN%2Bg94L._AC_UL1500_.jpg', 'Casio Edifice', 'This is a watch. A brand new Watch from VW. Waterproof, stainless steel !', 150, 0.1, 15, 4.6, 'female', 0];
$WatchThree[9] = $WatchThree[3] - ($WatchThree[3] * $WatchThree[4]);
$WatchThree[10] = $WatchThree[9] + $WatchThree[5];

$WatchFour = ['https://stockx.imgix.net/products/watches/Casio-G-Shock-GA-2110SU-9A-Black.png?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&q=90&dpr=2&trim=color&updated_at=1586358172', 'Casio Edifice', 'This is a watch. A brand new Watch from VW. Waterproof, stainless steel !', 650, 0, 15, 3.2, 'Male', 1];
$WatchFour[9] = $WatchFour[3] - ($WatchFour[3] * $WatchFour[4]);
$WatchFour[10] = $WatchFour[9] + $WatchFour[5];

$WatchFive = ['https://images.casiocdn.com/fit-in/368x500/casio-v2/resource/images/products/watches/hd/MTGB1000B-1A4_hd.png', 'Casio Edifice', 'This is a watch. A brand new Watch from VW. Waterproof, stainless steel !', 980, 0.7, 15, 5, 'female', 2];
$WatchFive[9] = $WatchFive[3] - ($WatchFive[3] * $WatchFive[4]);
$WatchFive[10] = $WatchFive[9] + $WatchFive[5];

$WatchSix = ['https://cdn11.bigcommerce.com/s-f06f69/images/stencil/1280x1280/products/9990/41862/GMW-B5000D-1AER-1__70484.1523953565.jpg?c=2&imbypass=on', 'Casio Edifice', 'This is a watch. A brand new Watch from VW. Waterproof, stainless steel !', 300, 0.4, 15, 2, 'female', 700];
$WatchSix[9] = $WatchSix[3] - ($WatchSix[3] * $WatchSix[4]);
$WatchSix[10] = $WatchSix[9] + $WatchSix[5];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Watch web shop</title>
    <style>
        .card-img-top {
            width: 100%;
            height: 50vh;
            object-fit: contain;
        }
    </style>
</head>

<body>

    <div class="container">
        <h2 class="text-center">Products</h2>
        <div class="row">
            <div class="col-md-4 mb-3">

                <div class="card">
                    <img src="<?php echo $WatchOne[0] ?>" class="card-img-top img-fluid p-3" alt="invalid link">
                    <div class="card-body" <?php if ($WatchOne[7] == 'female') {
                                                echo ('style="background: #ffe6ee"');
                                            } else {
                                                echo ('style="background:#f7f7f7"');
                                            } ?>>

                        <?php if ($WatchOne[4] == 0) { ?>
                            <h5 class="card-title"><?php echo $WatchOne[1] ?></h5>
                        <?php } else { ?>
                            <h5 class="card-title text-center text-uppercase font-weight-bold"><?php echo $WatchOne[1] . " " . $WatchOne[4] * 100 ?>&percnt; OFF</h5>
                        <?php } ?>
                        <p class="card-text"><?php echo $WatchOne[2] ?></p>
                        <ul class="list-unstyled">

                            <?php if ($WatchOne[4] > 0) { ?>
                                <div class="row">
                                    <div class="col-6">
                                        <p class="text-muted font-weight-bold"><s>&dollar;<?php echo $WatchOne[3] ?></s></p>
                                    </div>
                                    <div class="col-6 font-weight-bold">&dollar;<?php echo $WatchOne[9] ?></div>
                                </div>
                            <?php } else { ?>
                                <p class="font-weight-bold">&dollar;<?php echo $WatchOne[3] ?></p>
                            <?php } ?>


                            <li>Shipping:
                                <?php if ($WatchOne[5] > 0) { ?>
                                    <span>&dollar;<?php echo $WatchOne[5] ?></span>
                                <?php } else { ?>
                                    <span class="text-success font-weight-bold"><?php echo "Free shipping" ?> </span>
                                <?php } ?>
                            </li>

                            <li class="py-3">Rating: <?php echo $WatchOne[6] ?>/5</li>
                            <li>
                                <?php if ($WatchOne[8] > 0) { ?>
                                    
                                    <span class="text-success font-weight-bold"><?php echo $WatchOne[8] ?> items in stock</span>
                                <?php } else { ?>
                                    <span class="text-danger font-weight-bold">Out of stock</span>
                                <?php } ?>
                            </li>

                            <li class="py-3"> Total: &dollar; <?php echo $WatchOne[10] ?> </li>
                        </ul>
                        <?php if ($WatchOne[8] > 0) { ?>
                            <a href="#" class="btn btn-success w-100">Order</a>
                        <?php } else { ?>
                            <a href="#" class="btn btn-danger w-100">Not available</a>
                        <?php } ?>

                    </div>
                </div>
            </div>

            <div class="col-md-4">

                <div class="card">
                    <img src="<?php echo $WatchTwo[0] ?>" class="card-img-top img-fluid p-3" alt="invalid link">
                    <div class="card-body" <?php if ($WatchTwo[7] == 'female') {
                                                echo ('style="background: #ffe6ee"');
                                            } else {
                                                echo ('style="background:#f7f7f7"');
                                            } ?>>
                        <?php if ($WatchTwo[4] == 0) { ?>
                            <h5 class="card-title"><?php echo $WatchTwo[1] ?></h5>
                        <?php } else { ?>
                            <h5 class="card-title text-center text-uppercase font-weight-bold"><?php echo $WatchTwo[1] . " " . $WatchTwo[4] * 100 ?>&percnt; OFF</h5>
                        <?php } ?>
                        <p class="card-text"><?php echo $WatchTwo[2] ?></p>
                        <ul class="list-unstyled">
                            <?php if ($WatchTwo[4] > 0) { ?>
                                <div class="row">
                                    <div class="col-6">
                                        <p class="text-muted font-weight-bold"><s>&dollar;<?php echo $WatchTwo[3] ?></s></p>
                                    </div>
                                    <div class="col-6 font-weight-bold">&dollar;<?php echo $WatchTwo[9] ?></div>
                                </div>
                            <?php } else { ?>
                                <p class="font-weight-bold">&dollar;<?php echo $WatchTwo[3] ?></p>
                            <?php } ?>
                            <li>Shipping:
                                <?php if ($WatchTwo[5] > 0) { ?>
                                    <span>&dollar;<?php echo $WatchTwo[5] ?></span>
                                <?php } else { ?>
                                    <span class="text-success font-weight-bold"><?php echo "Free shipping" ?> </span>
                                <?php } ?>
                            </li>
                            <li class="py-3">Rating: <?php echo $WatchTwo[6] ?>/5</li>
                            <li>
                                <?php if ($WatchTwo[8] > 0) { ?>
                                    <span class="text-success font-weight-bold"><?php echo $WatchTwo[8] ?> items in stock</span>
                                <?php } else { ?>
                                    <span class="text-danger font-weight-bold">Out of stock</span>
                                <?php } ?>
                            </li>
                            <li class="py-3"> Total: &dollar; <?php echo $WatchTwo[10] ?> </li>
                        </ul>
                        <?php if ($WatchTwo[8] > 0) { ?>
                            <a href="#" class="btn btn-success w-100">Order</a>
                        <?php } else { ?>
                            <a href="#" class="btn btn-danger w-100">Not available</a>
                        <?php } ?>

                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card">
                    <img src="<?php echo $WatchThree[0] ?>" class="card-img-top img-fluid p-3" alt="invalid link">
                    <div class="card-body" <?php if ($WatchThree[7] == 'female') {
                                                echo ('style="background: #ffe6ee"');
                                            } else {
                                                echo ('style="background:#f7f7f7"');
                                            } ?>>
                        <?php if ($WatchThree[4] == 0) { ?>
                            <h5 class="card-title"><?php echo $WatchThree[1] ?></h5>
                        <?php } else { ?>
                            <h5 class="card-title text-center text-uppercase font-weight-bold"><?php echo $WatchThree[1] . " " . $WatchThree[4] * 100 ?>&percnt; OFF</h5>
                        <?php } ?>
                        <p class="card-text"><?php echo $WatchThree[2] ?></p>
                        <ul class="list-unstyled">
                            <?php if ($WatchThree[4] > 0) { ?>
                                <div class="row">
                                    <div class="col-6">
                                        <p class="text-muted font-weight-bold"><s>&dollar;<?php echo $WatchThree[3] ?></s></p>
                                    </div>
                                    <div class="col-6 font-weight-bold">&dollar;<?php echo $WatchThree[9] ?></div>
                                </div>
                            <?php } else { ?>
                                <p class="font-weight-bold">&dollar;<?php echo $WatchThree[3] ?></p>
                            <?php } ?>
                            <li>Shipping:
                                <?php if ($WatchThree[5] > 0) { ?>
                                    <span>&dollar;<?php echo $WatchThree[5] ?></span>
                                <?php } else { ?>
                                    <span class="text-success font-weight-bold"><?php echo "Free shipping" ?> </span>
                                <?php } ?>
                            </li>
                            <li class="py-3">Rating: <?php echo $WatchThree[6] ?>/5</li>
                            <li>
                                <?php if ($WatchThree[8] > 0) { ?>
                                    <span class="text-success font-weight-bold"><?php echo $WatchThree[8] ?> items in stock</span>
                                <?php } else { ?>
                                    <span class="text-danger font-weight-bold">Out of stock</span>
                                <?php } ?>
                            </li>
                            <li class="py-3"> Total: &dollar; <?php echo $WatchThree[10] ?> </li>
                        </ul>
                        <?php if ($WatchThree[8] > 0) { ?>
                            <a href="#" class="btn btn-success w-100">Order</a>
                        <?php } else { ?>
                            <a href="#" class="btn btn-danger w-100">Not available</a>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <img src="<?php echo $WatchFour[0] ?>" class="card-img-top img-fluid p-3" alt="invalid link">
                    <div class="card-body" <?php if ($WatchFour[7] == 'female') {
                                                echo ('style="background: #ffe6ee"');
                                            } else {
                                                echo ('style="background:#f7f7f7"');
                                            } ?>>
                        <?php if ($WatchFour[4] == 0) { ?>
                            <h5 class="card-title"><?php echo $WatchFour[1] ?></h5>
                        <?php } else { ?>
                            <h5 class="card-title text-center text-uppercase font-weight-bold"><?php echo $WatchFour[1] . " " . $WatchFour[4] * 100 ?>&percnt; OFF</h5>
                        <?php } ?>
                        <p class="card-text"><?php echo $WatchFour[2] ?></p>
                        <ul class="list-unstyled">
                            <?php if ($WatchFour[4] > 0) { ?>
                                <div class="row">
                                    <div class="col-6">
                                        <p class="text-muted font-weight-bold"><s>&dollar;<?php echo $WatchFour[3] ?></s></p>
                                    </div>
                                    <div class="col-6 font-weight-bold">&dollar;<?php echo $WatchFour[9] ?></div>
                                </div>
                            <?php } else { ?>
                                <p class="font-weight-bold">&dollar;<?php echo $WatchFour[3] ?></p>
                            <?php } ?>
                            <li>Shipping:
                                <?php if ($WatchFour[5] > 0) { ?>
                                    <span>&dollar;<?php echo $WatchFour[5] ?></span>
                                <?php } else { ?>
                                    <span class="text-success font-weight-bold"><?php echo "Free shipping" ?> </span>
                                <?php } ?>
                            </li>
                            <li class="py-3">Rating: <?php echo $WatchFour[6] ?>/5</li>
                            <li>
                                <?php if ($WatchFour[8] > 0) { ?>
                                    <span class="text-success font-weight-bold"><?php echo $WatchFour[8] ?> items in stock</span>
                                <?php } else { ?>
                                    <span class="text-danger font-weight-bold">Out of stock</span>
                                <?php } ?>
                            </li>
                            <li class="py-3"> Total: &dollar; <?php echo $WatchFour[10] ?> </li>
                        </ul>
                        <?php if ($WatchFour[8] > 0) { ?>
                            <a href="#" class="btn btn-success w-100">Order</a>
                        <?php } else { ?>
                            <a href="#" class="btn btn-danger w-100">Not available</a>
                        <?php } ?>

                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card">
                    <img src="<?php echo $WatchFive[0] ?>" class="card-img-top img-fluid p-3" alt="invalid link">
                    <div class="card-body" <?php if ($WatchFive[7] == 'female') {
                                                echo ('style="background: #ffe6ee"');
                                            } else {
                                                echo ('style="background:#f7f7f7"');
                                            } ?>>
                        <?php if ($WatchFive[4] == 0) { ?>
                            <h5 class="card-title"><?php echo $WatchFive[1] ?></h5>
                        <?php } else { ?>
                            <h5 class="card-title text-center text-uppercase font-weight-bold"><?php echo $WatchFive[1] . " " . $WatchFive[4] * 100 ?>&percnt; OFF</h5>
                        <?php } ?>
                        <p class="card-text"><?php echo $WatchFive[2] ?></p>
                        <ul class="list-unstyled">
                            <?php if ($WatchFive[4] > 0) { ?>
                                <div class="row">
                                    <div class="col-6">
                                        <p class="text-muted font-weight-bold"><s>&dollar;<?php echo $WatchFive[3] ?></s></p>
                                    </div>
                                    <div class="col-6 font-weight-bold">&dollar;<?php echo $WatchFive[9] ?></div>
                                </div>
                            <?php } else { ?>
                                <p class="font-weight-bold">&dollar;<?php echo $WatchFive[3] ?></p>
                            <?php } ?>
                            <li>Shipping:
                                <?php if ($WatchFive[5] > 0) { ?>
                                    <span>&dollar;<?php echo $WatchFive[5] ?></span>
                                <?php } else { ?>
                                    <span class="text-success font-weight-bold"><?php echo "Free shipping" ?> </span>
                                <?php } ?>
                            </li>
                            <li class="py-3">Rating: <?php echo $WatchFive[6] ?>/5</li>
                            <li>
                                <?php if ($WatchFive[8] > 0) { ?>
                                    <span class="text-success font-weight-bold"><?php echo $WatchFive[8] ?> items in stock</span>
                                <?php } else { ?>
                                    <span class="text-danger font-weight-bold">Out of stock</span>
                                <?php } ?>
                            </li>
                            <li class="py-3"> Total: &dollar; <?php echo $WatchFive[10] ?> </li>
                        </ul>
                        <?php if ($WatchFive[8] > 0) { ?>
                            <a href="#" class="btn btn-success w-100">Order</a>
                        <?php } else { ?>
                            <a href="#" class="btn btn-danger w-100">Not available</a>
                        <?php } ?>

                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card">
                    <img src="<?php echo $WatchSix[0] ?>" class="card-img-top img-fluid img-thumbnail p-3" alt="invalid link">
                    <div class="card-body" <?php if ($WatchSix[7] == 'female') {
                                                echo ('style="background: #ffe6ee"');
                                            } else {
                                                echo ('style="background:#f7f7f7"');
                                            } ?>>
                        <?php if ($WatchSix[4] == 0) { ?>
                            <h5 class="card-title"><?php echo $WatchSix[1] ?></h5>
                        <?php } else { ?>
                            <h5 class="card-title text-center text-uppercase font-weight-bold"><?php echo $WatchSix[1] . " " . $WatchSix[4] * 100 ?>&percnt; OFF</h5>
                        <?php } ?>
                        <p class="card-text"><?php echo $WatchSix[2] ?></p>
                        <ul class="list-unstyled">
                            <?php if ($WatchSix[4] > 0) { ?>
                                <div class="row">
                                    <div class="col-6">
                                        <p class="text-muted font-weight-bold"><s>&dollar;<?php echo $WatchSix[3] ?></s></p>
                                    </div>
                                    <div class="col-6 font-weight-bold">&dollar;<?php echo $WatchSix[9] ?></div>
                                </div>
                            <?php } else { ?>
                                <p class="font-weight-bold">&dollar;<?php echo $WatchSix[3] ?></p>
                            <?php } ?>
                            <li>Shipping:
                                <?php if ($WatchSix[5] > 0) { ?>
                                    <span>&dollar;<?php echo $WatchSix[5] ?></span>
                                <?php } else { ?>
                                    <span class="text-success font-weight-bold"><?php echo "Free shipping" ?> </span>
                                <?php } ?>
                            </li>
                            <li class="py-3">Rating: <?php echo $WatchSix[6] ?>/5</li>
                            <li>
                                <?php if ($WatchSix[8] > 0) { ?>
                                    <span class="text-success font-weight-bold"><?php echo $WatchSix[8] ?> items in stock</span>
                                <?php } else { ?>
                                    <span class="text-danger font-weight-bold">Out of stock</span>
                                <?php } ?>
                            </li>
                            <li class="py-3"> Total: &dollar; <?php echo $WatchSix[10] ?> </li>
                        </ul>
                        <?php if ($WatchSix[8] > 0) { ?>
                            <a href="#" class="btn btn-success w-100">Order</a>
                        <?php } else { ?>
                            <a href="#" class="btn btn-danger w-100">Not available</a>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
</body>

</html>